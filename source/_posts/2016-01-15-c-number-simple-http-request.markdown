---
layout: post
title: "C# Simple Http Request"
date: 2016-01-15 13:31:57 +0000
published: false
comments: true
categories: [http,c#,request,simple,library]
---

Http requests in c# are generally wordy. As in you'll have to type 10-15 lines for something that
shouldn't be so long. There are some libraries available in Microsoft Client API but normally
I find them to be error prone. So we'll create one here

## Static class
I prefer to place this form of functionality in my `Helpers` namespace in a static class:

```cpp
namespace myProject.Helpers {
    public static class Http {
        public static async Task<string> SimpleRequest(Uri address, string method, string contentType = null, WebHeaderCollection headers = null)
        {
            /*
             * Run in a new thread
             */
            return await Task.Run(() => {
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(address);
                webRequest.Method = method;
                webRequest.KeepAlive = true;
                if (contentType != null)
                    webRequest.ContentType = contentType;
                if (headers != null)
                    webRequest.Headers.AddRange(headers); // Described bellow

                try
                {
                    HttpWebResponse response = (HttpWebResponse) webRequest.GetResponse();
                    using (var stream = response.GetResponseStream())
                    {
                        var reader = new StreamReader(stream);
                        return reader.ReadToEnd();
                    }
                }
                catch (Exception e)
                {
                    Debug.Print("[!] Http Request Failed " + e.StackTrace);
                    return null;
                }
            });
        }
    }
}
```

The method AddRange simply takes `WebHeaderCollection` and adds it to an existing one. Here is the extension method
in the same class

```cpp
public static void AddRange(this WebHeaderCollection header, WebHeaderCollection range)
{
    foreach (string i in range)
    {
        header[i] = range[i];
    }
}
```

I caught any `HttpException` and returned null in the case of an error. You might want to catch the exception
in the context this method is being called in which is a better practice. Either way it's configurable to your
choosing. One thing to note this does not take parameters for `POST` data. If you wish to extend it to do so
post in the comment.
