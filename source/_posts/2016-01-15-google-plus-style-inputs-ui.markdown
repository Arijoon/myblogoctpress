---
layout: post
title: "Google plus style inputs UI"
date: 2016-01-15 10:16:46 +0000
comments: true
published: false
categories: [ui,html,css,js,googleplus,form,ux]
---

I'll be explaining how to accomplish a similar form editing UI as google plus. My version is a little different so feel free to modify the styles to match your needs. 

## Form fields
Forms fields are made up of a label (`absolutely` positioned on top) and an `input` field on the bottom .For the underline we use another div tag which is `absolutely` positioned. Let's div into the source and show this. The following is the markup:

```html
<form>
	<div class="form-group input-group-wrapper">
	    <div class="input-wrapper">
	        <input type="text" name="firstName" class="form-control input-field" required="required" />
	        <label class="placeholder input-label" for="firstName">First Name</label>
	        <div class="absolute-wrapper">
	            <div class="underline"></div>
	        </div>
	    </div>
	</div>
</form>
```

Let's style that with some less code ([css compiled version at the bottom of the page](#css-version)):

```css
form {    
    input[type="text"] ,
    input[type="password"] ,
    input[type="email"] {
        padding: 2px inherit;
        background: none;
        outline: 0;
        font-size: 22px;
        font-weight: bold;
        &:focus ,&, &:active {
            border-top: none;
            border-left: none;
            border-right: none;
            border-radius: 0;
            outline: none !important;
            -webkit-appearance:none;
            -moz-box-shadow: none;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-bottom: 2px solid #E1E1E1;
        }
    }
} 
.input-group-wrapper { 
    .input-wrapper {
        padding-top: 25px;
        position: relative;
        overflow: hidden;
        .placeholder.input-label {
            .smx-transition(top .3s);
            position: absolute;
            top: 25px;
            left: 25px; // input field padding left + wrapper padding left
            line-height: 25px;
        }
    }
    &.input-has-content {
        .placeholder.input-label {
            top: 0;
        }
    }
}

.placeholder {
    font-weight: 400;
    font-size: 18px;
    color: #4a4a4a;
    font-family: OpenSans-Light;
}
```

If you're not using less, you can compile that to css using a range of online less compilers. That code also assumes you're using bootstrap so we'll have to override some of their styling.


Now we need to style the underline animation:

```css
// underline mixin
.underline(@animation: none, @color: blue, @position: relative) {
    position: @position;
    border-top: 2px solid @color;
    width: 0;
    margin: 0;
    -webkit-transition: @animation;
    -moz-transition: @animation;
    -o-transition: @animation;
    transition: @animation;
}

// change 0.5s to make it slower or faster
div.underline {
    .underline(width 0.5s, blue, absolute);
    bottom: 0;
    left: 0;
    &.active {
        width: 100%;
        left: 0;
    }
}

.absolute-wrapper {
    height: 100%;
    position: relative;
```

### Using Javascript for interaction
You can use javascript to directly edit the styles but I prefer to add classes and avoid direct style attributes in `js` code. 

```js
/*
* Move the form label up on click
* This must be ran inside $(document).on('ready' ...
*/
(function formLabelInteraction() {
	// Activate underline and move text by
   $('.input-group-wrapper').on('focusin', function () {
       $(this).addClass('input-has-content');
       $(this).find('.underline').addClass('active');
   });

	// Deactivate underline and move text back if no content
   $('.input-group-wrapper').on('focusout', function () {
       if (!$(this).find('input.input-field').val())
           $(this).removeClass('input-has-content');

       $(this).find('.underline').removeClass('active');
   });

	// Activate if even label clicked or anything in input-wrapper
   $('.input-wrapper').on('click', function () {
       $(this).find('input.input-field').focus();
   });

	// Checks if input fields already have values
   (function checkForContent() {
       $('input.input-field').each(function (index) {
           if ($(this).val())
               $(this).closest('.input-group-wrapper').addClass('input-has-content');
       });
   })();
})();
```

Use this [jsFiddle](https://jsfiddle.net/Lm59hfnv/) to see it live.

Here is the compiled css: <a name="css-version"></a>
```css
div.underline {
  position: absolute;
  border-top: 2px solid #0000ff;
  width: 0;
  margin: 0;
  -webkit-transition: width 0.5s;
  -moz-transition: width 0.5s;
  -o-transition: width 0.5s;
  transition: width 0.5s;
  bottom: 0;
  left: 0;
}
div.underline.active {
  width: 100%;
  left: 0;
}
.absolute-wrapper {
  height: 100%;
  position: relative;
}
form input[type="text"],
form input[type="password"],
form input[type="email"] {
  padding: 2px inherit;
  background: none;
  outline: 0;
  font-size: 22px;
  font-weight: bold;
}
form input[type="text"]:focus,
form input[type="password"]:focus,
form input[type="email"]:focus,
form input[type="text"],
form input[type="password"],
form input[type="email"],
form input[type="text"]:active,
form input[type="password"]:active,
form input[type="email"]:active {
  border-top: none;
  border-left: none;
  border-right: none;
  border-radius: 0;
  outline: none !important;
  -webkit-appearance: none;
  -moz-box-shadow: none;
  -webkit-box-shadow: none;
  box-shadow: none;
  border-bottom: 2px solid #E1E1E1;
}
.input-group-wrapper .input-wrapper {
  padding-top: 25px;
  position: relative;
  overflow: hidden;
}
.input-group-wrapper .input-wrapper .placeholder.input-label {
  -webkit-transition: top 0.3s;
  -moz-transition: top 0.3s;
  -o-transition: top 0.3s;
  transition: top 0.3s;
  position: absolute;
  top: 25px;
  left: 25px;
  line-height: 25px;
}
.input-group-wrapper.input-has-content .placeholder.input-label {
  top: 0;
}
.placeholder {
  font-weight: 400;
  font-size: 18px;
  color: #4a4a4a;
  font-family: OpenSans-Light;
}
```
