<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title><![CDATA[Category: Http | My Blog]]></title>
  <link href="http://yaraee.net/blog/categories/http/atom.xml" rel="self"/>
  <link href="http://yaraee.net/"/>
  <updated>2016-03-05T17:55:02+00:00</updated>
  <id>http://yaraee.net/</id>
  <author>
    <name><![CDATA[Arman Yaraee]]></name>
    <email><![CDATA[contact@yaraee.net]]></email>
  </author>
  <generator uri="http://octopress.org/">Octopress</generator>

  
  <entry>
    <title type="html"><![CDATA[Sending Files From One Asp Application to Another]]></title>
    <link href="http://yaraee.net/2016/01/15/sending-files-from-one-asp-application-to-another/"/>
    <updated>2016-01-15T14:32:53+00:00</updated>
    <id>http://yaraee.net/2016/01/15/sending-files-from-one-asp-application-to-another</id>
    <content type="html"><![CDATA[<p>In this post I&rsquo;ll explain how you can send files from one ASP application to another. I&rsquo;ll
be using <code>HttpClient</code> class to make it simpler to send requests. This post is split into two
sections of:</p>

<ul>
<li>Sending a request with File</li>
<li>Receiving a request with File</li>
</ul>


<p>This is for API calls so I won&rsquo;t be using <code>HttpPostedFileBase</code>.</p>

<h3>Send a <code>POST</code> request with file content</h3>

<hr />

<p>So first thing we need to do is read a file:</p>

<pre><code class="cpp">var filename = "file_name.jpg";
var bytes = File.ReadAllBytes("path\to\file\" + filename);
</code></pre>

<p>Now we&rsquo;ll write the file bytes to a stream:</p>

<pre><code class="cpp">var stream = new MemoryStream();
stream.Write(bytes, 0, bytes.Length);
</code></pre>

<p>At this point we&rsquo;re ready to create our request and send it. I&rsquo;ll add the filename to the request
headers. You can even add it to the url as a query string.</p>

<pre><code class="cpp">// VERY IMPORTANT to rewind the stream to the beginning
stream.Seek(0, SeekOrigin.Begin)

// Create Stream objects
var client = new HttpClient();
var content = new StreamContent(stream);

// Add headers
content.Headers.Add("Content-Type", "image/jpeg");
content.Headers.Add("File-Name", filename; // Add filename to headers

// Post the request
// _request_uri should be the path your endpoint
var result = client.PostAsync(_requestUri, content).Result;
</code></pre>

<h3>Receive the <code>POST</code> request with file content</h3>

<hr />

<p>Receiving the request is much easier. All we have to do is read the content of the
<code>POST</code> request:</p>

<pre><code class="cpp">[HttpPost]
public async Task&lt;HttpResponseMessage&gt; Post()
{
    var data = await Request.Content.ReadAsStreamAsync();
}
</code></pre>

<p>Now you can do anything you want with the file stream stored in data. You can get the <code>File-Name</code> as follows:</p>

<pre><code class="cpp">IEnumerable&lt;string&gt; values;
if (!Request.Headers.TryGetValues("File-Name", out values))
{
    // If filename does not exist return error message
    return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
}
var filename = values.First&lt;string&gt;();
</code></pre>

<h3>Data cleanup</h3>

<hr />

<p>Remember to cleanup your disposable objects or nest them in <code>using</code> statements:</p>

<pre><code class="cpp">using(var stream = new MemoryStream()) {
    // operations
    using(var client = HttpClient()) {
        using(var content = new StreamContent(stream)) {
            // content operations and sending the request
        }
    }
}
</code></pre>

<p>Or explicitly dispose them:
<code>cpp
client.Dispose();
content.Dispose();
stream.Dispose();
</code></p>
]]></content>
  </entry>
  
</feed>
